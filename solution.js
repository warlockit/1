function discriminant (a, b, c) {
	return Math.pow(b,2) - 4*a*c;
}

function isFloat(num) {return parseInt( num ) !== parseFloat( num )}

function solution (a, b, c) {
	var d = discriminant(a, b, c); // Находим дискриминант
	var x1, x2;
	var roots = [];

	// Выбор типа квадратного уравнения и решение по соответствуюшей формуле 
	if (a != 0) {
		if (b != 0 && c != 0) {
			if (d > 0) {
				x1 = (- b + Math.sqrt(d))/(2*a);
				x2 = (- b - Math.sqrt(d))/(2*a);

				if (isFloat(x1)) {
					x1 = drawFraction(-b + " + &radic;" + d, (2*a));
				}
				if (isFloat(x2)) {
					x2 = drawFraction(-b + " - &radic;" + d, (2*a));
				}

				roots.push(x1, x2);
			} else if (d === 0) {
				x1 = -b/(2*a);
				roots.push(x1);
			}
		} else if (b == 0 && c != 0) {
			x1 = Math.sqrt(-c/a);
			x2 = - Math.sqrt(-c/a);
			roots.push(x1, x2);
		} else if (b != 0 && c == 0) {
			x1 = -b/a;
			x2 = 0;
			roots.push(x1, x2);
		} else if (b == 0 && c == 0) {
			roots.push(0);
		}
	} else {
		alert("Это не квадратное уравнение");
	}

	return roots;
}

function displaySolution (roots) {
	switch (roots.length) {
		case 1:
			return "<div class=\"answer\"><span>Ответ: x =&nbsp;</span>" + "<span>" + roots[0] + "</span></div>";
		break;
		case 2:
			return "<div class=\"answer\"><span>Ответ: x<sub>1</sub> =&nbsp;</span>" + "<span>" + roots[0] + "</span>" + 
				   "<span>, x<sub>2</sub> =&nbsp;</span>" + "<span>" + roots[1] + "</span></div>";
		break;
		default:
			return "Решений нет";
	}
}

function drawFraction (numerator, denominator) {
	return "<div class=\"fraction\"><div class=\"numerator\">" + numerator +
		   "</div><div class=\"denominator\">" + denominator + "</div></div>";
}

function clickButton () {
	var form = document.forms.equation;
	var a = parseInt(form.a.value) || 0;
	var b = parseInt(form.b.value) || 0;
	var c = parseInt(form.c.value) || 0;
	var result = document.getElementById("result");
	var roots = solution(a, b, c);
	var html = "";

	html += displaySolution(roots);

	result.innerHTML = html;
}

window.onload = function() {
   document.getElementById('solution').onclick = clickButton;
}

