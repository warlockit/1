window.onload = function () {
	document.getElementById("solution").onclick = clickButton;
}

function isFloat(num) {
	return parseInt( num ) !== parseFloat( num );
}

function clickButton () {
	var a = document.forms.equation.a.value;
	var b = document.forms.equation.b.value;
	var c = document.forms.equation.c.value;
	var roots = solution(a, b, c);
	var result = document.getElementById("result");
	
	result.innerHTML = displaySolution(roots);
}

function solution(a, b, c) {
	var d = b*b - 4*a*c;
	var x1, x2;
	var roots = [];
	
	if (a != 0) {
		if (b != 0 && c != 0) {
			if (d > 0) {
				x1 = ((-b + Math.sqrt(d))/(2*a));
				x2 = ((-b - Math.sqrt(d))/(2*a));

				if (isFloat(x1)) {
					x1 = "( - " + b + "+ &radic;" + d + ")/" + (2 * a);
				}
				if (isFloat(x2)) {
					x2 = "( - " + b + "- &radic;" + d + ")/" + (2 * a);
				}

				roots.push(x1, x2);

			} else if (d == 0) {
				x1 = -b/(2*a);
				roots.push(x1);
			}
		} else if (b == 0 && c != 0) {
			x1 = Math.sqrt(-c/a);
			x2 = - Math.sqrt(-c/a);
			roots.push(x1, x2);
		} else if (b != 0 && c == 0) {
			x1 = -b/a;
			x2 = 0;
		}

	} else {
		alert("Данное уравнение не квадратное");
	}

	return roots;
}

function displaySolution(roots) {
	switch (roots.length) {
		case 1:
			return "Ответ: x<sub>1</sub> = " + roots[0];
		break;
		case 2:
			return "Ответ: x<sub>1</sub> = " + roots[0] + ", x<sub>2</sub> = " + roots[1];
		break;
		default:
			return "Ответ: Решений нет";
	}
}

function drawFraction (numerator, denominator) {
	return "<div class=\"fraction\"><div class=\"numerator\">" + numerator +
		   "</div><div class=\"denominator\">" + denominator + "</div></div>";
}